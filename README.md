# go-examples

Go examples of various things.

## Set Up

After cloning or downloading, files are run with go run, 
unless otherwise stated.

```
go run "filename.go" # run file
```

## License
[MIT](https://choosealicense.com/licenses/mit/)

