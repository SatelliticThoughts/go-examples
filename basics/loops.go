package main

import "fmt"

func main() {
	running := true
	num := 0

	for running {
		num += 1
		if num > 5 {
			fmt.Println("Exiting loop...")
			running = false
		}
	}

	for i := 0; i < 13; i++ {
		fmt.Printf("\n6 + %v = %v", i, i + 6)
	}

	fmt.Println()

	words := []string{"word", "test", "cat"}
	for _, word := range words {
		fmt.Println(word)
	}
}
