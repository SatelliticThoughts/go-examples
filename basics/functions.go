package main

import "fmt"

func sayHello() {
	fmt.Println("Hello")
}

func getWord() string {
	return "dog"
}

func doubleNumber(x int) {
	fmt.Printf("%v\n", x * 2)
}

func addNumbers(x int, y int) int {
	return x + y
}

func main() {
	sayHello()
	fmt.Println(getWord())
	doubleNumber(4)
	fmt.Printf("%v\n", addNumbers(3, 2))
}

