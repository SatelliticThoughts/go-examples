package main

import ("fmt")

func main() {
	var x = 6
	var y = 4

	fmt.Printf("Integer Calculations; x = %v, y = %v", x, y)

	fmt.Printf("\nx + y = %v", x + y)
	fmt.Printf("\nx - y = %v", x - y)
	fmt.Printf("\nx * y = %v", x * y)
	fmt.Printf("\nx / y = %v", x / y)
	fmt.Printf("\nx mod y = %v", x % y)

	var a = 6.12
	var b = 4.01

	fmt.Printf("Floating point Calculations; x = %v, y = %v", x, y)

	fmt.Printf("\na + b = %v", a + b)
	fmt.Printf("\na - b = %v", a - b)
	fmt.Printf("\na * b = %v", a * b)
	fmt.Printf("\na / b = %v", a / b)

	fmt.Println("String Manipulation")

	var hello = "hello"
	fmt.Println(hello)

	hello += " World"
	fmt.Println(hello)

	fmt.Printf("length: %v\n", len(hello))
	fmt.Println(hello[2:5])

	fmt.Println("\nArrays");

	numbers := []int{3, 2, 1, 8, 8}
	for i := 0; i < len(numbers); i++ {
		fmt.Printf("%v, ", numbers[i])
	}

	fmt.Println()

	temp := []int{1, 1, 1, 1}
	numbers = append(numbers, temp...)
	for i := 0; i < len(numbers); i++ {
		fmt.Printf("%v, ", numbers[i])
	}
}

