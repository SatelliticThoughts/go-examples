package main

import "fmt"

type Person struct {
	name string
	age int
}

func main() {
	p1 := Person{
		name: "Paul",
		age: 301,
	}

	p2 := Person{
		name: "Not Paul",
		age: 1421,
	}

	fmt.Printf("person: %v, %v\n", p1.name, p1.age)
	fmt.Printf("person: %v, %v\n", p2.name, p2.age)
}

