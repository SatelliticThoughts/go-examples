package main

import (
	"fmt"
	"strconv"
	)

func main() {
	fmt.Println("Hello World!!!")

	var text string = "This is a test";
	fmt.Println(text);

	var name string = "Luke";
	var age int = 45;
	fmt.Println("My name is " + name + " and is " + strconv.Itoa(age) + " years old");
}

