package main

import "fmt"

func main() {
	if true {
		fmt.Println("This will show")
	}

	if false {
		fmt.Println("This won't show")
	} else {
		fmt.Println("This will show")
	}

	if false {
		fmt.Println("This won't show")
	} else if true {
		fmt.Println("This will show")
	} else {
		fmt.Println("This won't show")
	}

	if true && true {
		fmt.Println("This will show")
	}

	if true && false {
		fmt.Println("This won't show")
	}

	if false && false {
		fmt.Println("This won't show")
	}

	if true || true {
		fmt.Println("This will show")
	}

	if true || false {
		fmt.Println("This will show")
	}

	if false || false {
		fmt.Println("This won't show")
	}

	x := 4
	y := 8

	if x == y {
		fmt.Println("x equals y")
	}

	if x != y {
		fmt.Println("x doesn't equal y")
	}

	if x > y {
		fmt.Println("x greater than y")
	}

	if x < y {
		fmt.Println("x less than y")
	}
}

